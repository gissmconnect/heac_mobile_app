import {Dimensions, Platform, StatusBar, PixelRatio} from 'react-native';
import {getStatusBarHeight} from '@utils/isIphoneX';

export const isIos = Platform.OS === 'ios';
export const isAndroid = Platform.OS === 'android';

const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;

export const colors = {
  tint: '#7B72AF',
};

export const rgbaColor = {
  white: '255, 255, 255',
  tint: '123, 114, 175',
  borderDarkGrey: '126, 137, 137',
  textGrey: '142, 142, 147',
  lightBlack: '28, 28, 30',
  black: '0, 0, 0',
};

export const getRgbaColor = (color, opacity) => {
  return `rgba(${rgbaColor[color]}, ${opacity})`;
};

export const isIphoneX = () => {
  const dimen = Dimensions.get('window');
  return isIos
    ? dimen.height === 812 ||
        dimen.width === 812 ||
        dimen.height === 896 ||
        dimen.width === 896
    : dimen.height > 736;
};

export const isIphone6 = () => {
  const dimen = Dimensions.get('window');
  return dimen.height > 600 && dimen.height < 750;
};

const widthPercentageToDP = (
  iphoneWidthPercent,
  androidWidthPercent = iphoneWidthPercent,
) => {
  const elemWidth =
    typeof iphoneWidthPercent === 'number'
      ? isIos
        ? iphoneWidthPercent
        : androidWidthPercent
      : parseFloat(isIos ? iphoneWidthPercent : androidWidthPercent);
  return PixelRatio.roundToNearestPixel((SCREEN_WIDTH * elemWidth) / 100);
};

export const HeightPercentageToDP = (
  iphoneHeightPercent,
  androidHeightPercent = iphoneHeightPercent,
) => {
  const elemHeight =
    typeof iphoneHeightPercent === 'number'
      ? isIos
        ? iphoneHeightPercent
        : androidHeightPercent
      : parseFloat(isIos ? iphoneHeightPercent : androidHeightPercent);
  return PixelRatio.roundToNearestPixel((SCREEN_HEIGHT * elemHeight) / 100);
};

export const deviceDimensions = {
  width: SCREEN_WIDTH,
  height: SCREEN_HEIGHT,
  WPTDP: widthPercentageToDP,
  HPTDP: HeightPercentageToDP,
  WHRatio: SCREEN_WIDTH / SCREEN_HEIGHT,
};

export const heiWidScale = 0.125;

export const heiWidScale2 = deviceDimensions.WHRatio < 0.5 ? 0.125 : 0.15;

export const scales2 = size => {
  return deviceDimensions.HPTDP(heiWidScale2 * size);
};

export const dimensions = {
  statusBar: getStatusBar(true),
};

function getStatusBar() {
  return !isIos ? StatusBar.currentHeight : getStatusBarHeight();
}

export const fontSize = size => {
  const fontScale = deviceDimensions.WHRatio < 0.5 ? 0.123 : 0.15;
  return deviceDimensions.HPTDP(fontScale * size);
};

export const fontSizes = {
  extraLarge: fontSize(72), // 72
  large: fontSize(58), // 58
  heading: fontSize(33),
  title: fontSize(28),
  h1: fontSize(25),
  h2: fontSize(24),
  h3: fontSize(22),
  h4: fontSize(21),
  h5: fontSize(20),
  h6: fontSize(19),
  h7: fontSize(17),
  body1: fontSize(18), // 18
  body2: fontSize(16),
  body3: fontSize(15),
  subtitle1: fontSize(14),
  subtitle2: fontSize(13),
  caption: fontSize(12),
  small: fontSize(10),
};

export const marginPaddingScale = 0.125;
export const svgWidthScale = 0.122;
export const svgHeightScale = 0.126;

export const spacing = size => {
  return deviceDimensions.HPTDP(marginPaddingScale * size);
};

export const stringToHslColor = (str = 'Cash', s = 30, l = 60) => {
  var hash = 0;
  for (var i = 0; i < str.length; i++) {
    hash = str.charCodeAt(i) + ((hash << 5) - hash);
  }
  var h = hash % 360;
  return 'hsl(' + h + ', ' + s + '%, ' + l + '%)';
};
