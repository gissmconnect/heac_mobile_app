import React from 'react';
import {NavigationContainer, StackActions} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import HomeScreen from '@screens/home';

export const navigationRef = React.createRef();
export const isNavigationReadyRef = React.createRef();

export function navigate(name, params) {
  if (isNavigationReadyRef && isNavigationReadyRef.current) {
    navigationRef.current?.navigate(name, params);
  }
}

export function replace(...args) {
  if (isNavigationReadyRef && isNavigationReadyRef.current) {
    navigationRef.current?.dispatch(StackActions.replace(...args));
  }
}

const Stack = createNativeStackNavigator();

const RootNavigation = () => {
  return (
    <NavigationContainer ref={navigationRef}>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={HomeScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default RootNavigation;
