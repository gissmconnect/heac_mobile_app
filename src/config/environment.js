const _Environments = {
  production: {
    IS_DEV: false,
    BASE_URL: 'Config.BASE_URL',
  },

  development: {
    IS_DEV: true,
    BASE_URL: 'Config.BASE_URL',
  },
};

const getEnvironment = () => {
  const platform = getPlatform();
  return _Environments[platform];
};

const getPlatform = () => {
  let environment = '';
  if (__DEV__) {
    environment = 'development';
  } else {
    environment = 'production';
  }
  return environment;
};

export const Environment = getEnvironment();
