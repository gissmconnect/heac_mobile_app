module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      'module-resolver',
      {
        root: ['./src'],
        extensions: ['.ios.js', '.android.js', '.ts', '.jsx', '.js', '.json'],
        alias: {
          '@screens': './src/screens',
          '@navigation': './src/navigation',
          '@components': './src/components',
          '@config': './src/config',
          '@assets': './assets',
          '@utils': './src/utils',
          '@actions': './src/redux/actions',
          '@constants': './src/constants',
          '@context': './src/context',
          '@hoc': './src/hoc',
          '@action-types': './src/redux/constants/action-types',
          '@__tests__': './__tests__',
        },
      },
    ],
  ],
  env: {
    production: {
      plugins: ['transform-remove-console'],
    },
  },
};
